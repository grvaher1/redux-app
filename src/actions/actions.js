import {
  LOGIN,
  LOGOUT,
  ADD_PERSON,
  UPDATE_PERSON
} from './actionTypes';

export const login = () => {
    return {
      type: LOGIN
    };
  };
  
  export const logout = () => {
    return {
      type: LOGOUT
    };
  };
  
  export const addPerson = (person) => {
    return {
      type: ADD_PERSON,
      payload: person
    };
  };
  
  export const updatePerson = (person) => {
    return {
      type: UPDATE_PERSON,
      payload: person
    };
  };
  