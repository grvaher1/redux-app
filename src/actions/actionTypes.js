export const LOGIN = 'LOGIN';
export const LOGOUT = 'LOGOUT';
export const ADD_PERSON = 'ADD_PERSON';
export const UPDATE_PERSON = 'UPDATE_PERSON';