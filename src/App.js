import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { Provider } from 'react-redux';
import store from './redux/store';
import Login from './components/Login/Login';
import Home from './components/Home/Home';
import Profile from './components/Profile';
import PersonForm from './components/PersonForm/PersonForm';
import PrivateRoute from './components/PrivateRoute';

function App() {
  return (
    <Provider store={store}>
      <Router>
        <Routes>
          <Route path="/login" element={<Login />} />
          <Route path='/home' element={<PrivateRoute />}>
            <Route path='/home' element={<Home />} />
          </Route>
          <Route path='/profile/:id' element={<PrivateRoute />}>
            <Route path='/profile/:id' element={<Profile />} />
          </Route>
          <Route path='/new-person' element={<PrivateRoute />}>
            <Route path='/new-person' element={<PersonForm />} />
          </Route>
        </Routes>
      </Router>
    </Provider>
  );
}

export default App;
