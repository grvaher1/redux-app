import {
  LOGIN,
  LOGOUT,
  ADD_PERSON,
  UPDATE_PERSON
} from '../actions/actionTypes';

const initialState = {
  persons: [
    { id: 1, name: 'John Doe', age: 30, profession: 'Engineer' },
    { id: 2, name: 'Jane Smith', age: 25, profession: 'Designer' }
  ],
  loggedIn: false
};

export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN:
      return { ...state, loggedIn: true };
    case LOGOUT:
      return { ...state, loggedIn: false };
    case ADD_PERSON:
      return { ...state, persons: [...state.persons, action.payload] };
    case UPDATE_PERSON:
      return {
        ...state,
        persons: state.persons.map(person =>
          person.id === action.payload.id ? action.payload : person
        )
      };

    default:
      return state;
  }
};
