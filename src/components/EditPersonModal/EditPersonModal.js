import React, { useState } from 'react';
import { connect } from 'react-redux';
import { updatePerson } from '../actions/personActions';

const EditPersonModal = ({ person, updatePerson, closeModal }) => {
  const [name, setName] = useState(person.name);
  const [age, setAge] = useState(person.age);
  const [profession, setProfession] = useState(person.profession);

  const handleSubmit = (e) => {
    e.preventDefault();
    const updatedPerson = { id: person.id, name, age, profession };
    updatePerson(updatedPerson);
    closeModal();
  };

  return (
    <div>
      <h2>Edit Person</h2>
      <form onSubmit={handleSubmit}>
        <label>
          Name:
          <input type="text" value={name} onChange={(e) => setName(e.target.value)} />
        </label>
        <label>
          Age:
          <input type="text" value={age} onChange={(e) => setAge(e.target.value)} />
        </label>
        <label>
          Profession:
          <input type="text" value={profession} onChange={(e) => setProfession(e.target.value)} />
        </label>
        <button type="submit">Save</button>
      </form>
    </div>
  );
};

const mapDispatchToProps = (dispatch) => {
  return {
    updatePerson: (person) => dispatch(updatePerson(person))
  };
};

export default connect(null, mapDispatchToProps)(EditPersonModal);
