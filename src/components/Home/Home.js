import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Link as RouterLink, useNavigate } from 'react-router-dom';
import { Typography, Button, Table, TableHead, TableBody, TableRow, TableCell, Link } from '@mui/material';
import './Home.css';

const Home = () => {
  const persons = useSelector(state => state.persons);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const handleLogout = () => {
    localStorage.removeItem('loggedIn');
    dispatch({ type: 'LOGOUT' });
    navigate('/login');
  };

  return (
    <div className="home-container">
      <div className="header">
        <Typography variant="h3" className="title">Home</Typography>
        <Button color='error' variant="contained" onClick={handleLogout} sx={{ marginBottom: '20px' }}>Logout</Button>
      </div>
      <Table className="table">
        <TableHead>
          <TableRow>
            <TableCell>Name</TableCell>
            <TableCell>Age</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {persons.map(person => (
            <TableRow key={person.id}>
              <TableCell>
                <Link component={RouterLink} to={`/profile/${person.id}`} color="primary">
                  {person.name}
                </Link>
              </TableCell>
              <TableCell>{person.age}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
      <Button component={RouterLink} to="/new-person" variant="contained" color="primary" style={{ marginTop: '20px' }}>
        Add New Person
      </Button>
    </div>
  );
};

export default Home;