import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { Typography, Button, TextField } from '@mui/material';
import { UPDATE_PERSON, ADD_PERSON } from '../../actions/actionTypes';
import './PersonForm.css';

const PersonForm = ({ initialValues, onClose }) => {
  const persons = useSelector(state => state.persons);
  const [name, setName] = useState(initialValues ? initialValues.name : '');
  const [age, setAge] = useState(initialValues ? initialValues.age : '');
  const [profession, setProfession] = useState(initialValues ? initialValues.profession : '');
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const handleSubmit = (e) => {
    e.preventDefault();
    const newPerson = {
      name,
      age,
      profession
    };
    if (initialValues) {
      dispatch({ type: UPDATE_PERSON, payload: { ...initialValues, ...newPerson } });
    } else {
      dispatch({ type: ADD_PERSON, payload: { ...newPerson, id: persons.length + 1 } });
    }
    handleCancel()
  };

  const handleCancel = () => {
    onClose ? onClose() : navigate('/home');
  }

  return (
    <div className="person-form-container">
      <Typography variant="h4" className="form-title">{initialValues ? 'Edit Person' : 'Add New Person'}</Typography>
      <form onSubmit={handleSubmit} className="person-form">
        <TextField
          label="Name"
          variant="outlined"
          value={name}
          onChange={(e) => setName(e.target.value)}
          className="form-input"
        />
        <TextField
          label="Age"
          variant="outlined"
          type="number"
          value={age}
          onChange={(e) => setAge(e.target.value)}
          className="form-input"
        />
        <TextField
          label="Profession"
          variant="outlined"
          value={profession}
          onChange={(e) => setProfession(e.target.value)}
          className="form-input"
        />
        <Button type="submit" variant="contained" disabled={!name} className="form-button">{initialValues ? 'Save' : 'Add'}</Button>
        <Button variant="contained" onClick={handleCancel} color="secondary" className="form-button">Cancel</Button>
      </form>
    </div>
  );
};

export default PersonForm;