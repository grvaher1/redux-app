import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { useParams, useNavigate } from 'react-router-dom';
import { Typography, Button, Box } from '@mui/material';
import PersonForm from '../PersonForm/PersonForm';
import './Profile.css';

const Profile = () => {
  const { id } = useParams();
  const person = useSelector(state =>
    state.persons.find(person => person.id === parseInt(id))
  );
  const navigate = useNavigate();
  const [editing, setEditing] = useState(false);

  const handleEdit = () => {
    setEditing(true);
  };

  const handleOnClose = () => {
    setEditing(false);
  };

  const handleGoBack = () => {
    navigate('/home');
  };

  return (
    <div className="profile-container">
      <Typography variant="h3" className="profile-heading">Profile</Typography>
      <Box className="profile-box">
        {editing ? (
          <PersonForm initialValues={person} onClose={handleOnClose} />
        ) : (
          <>
            <Typography>Name: {person.name}</Typography>
            <Typography>Age: {person.age}</Typography>
            <Typography>Profession: {person.profession}</Typography>
            <Button variant="contained" onClick={handleEdit} sx={{ marginTop: '10px' }}>Edit Person</Button>
            <Button variant="contained" onClick={handleGoBack} sx={{ marginTop: '10px', marginLeft: '10px' }} color="secondary">Back</Button>
          </>
        )}
      </Box>
    </div>
  );
};

export default Profile;
