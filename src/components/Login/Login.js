import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { useLocation, useNavigate } from 'react-router-dom';
import { Typography, TextField, Button, Box } from '@mui/material';
import './Login.css';

const Login = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const location = useLocation();

  useEffect(() => {
    const params = new URLSearchParams(location.search);
    const initialUsername = params.get('initialUsername');
    const initialPassword = params.get('initialPassword');

    if (initialUsername && initialPassword) {
      localStorage.setItem('initialUsername', initialUsername);
      localStorage.setItem('initialPassword', initialPassword);
      navigate('/login');
    }
    // http://localhost:3000/login?initialUsername=myusername&initialPassword=mypassword
    // use above url in address bar then login with 'myusername' and 'mypassword'
  }, [location.search, navigate]);

  const handleLogin = () => {
    const storedUsername = localStorage.getItem('initialUsername');
    const storedPassword = localStorage.getItem('initialPassword');

    if (username === storedUsername && password === storedPassword) {
      localStorage.setItem('loggedIn', 'true');
      dispatch({ type: 'LOGIN' });
      navigate('/home');
    } else {
      setError('Invalid username or password.');
    }
  };

  return (
    <div className="login-container">
      <Typography variant="h3" fontWeight="bold" sx={{ marginBottom: '20px' }}>Login</Typography>
      <Box className="login-box">
        <TextField
          label="Username"
          variant="outlined"
          value={username}
          onChange={(e) => setUsername(e.target.value)}
          fullWidth
          margin="normal"
        />
        <TextField
          label="Password"
          type="password"
          variant="outlined"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          fullWidth
          margin="normal"
        />
        <Button variant="contained" onClick={handleLogin} fullWidth sx={{ marginTop: '10px' }}>Login</Button>
        {error && <Typography color="error" sx={{ marginTop: '10px' }}>{error}</Typography>}
      </Box>
    </div>
  );
};

export default Login;
